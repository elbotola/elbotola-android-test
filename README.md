## Introduction
The coding challenge(s) below will be used to assess your familiarity with the Android
development environment, relevant Android related frameworks and general coding best
practices. There are no hard time limits, but we’re trying to keep the scope limited so we don’t
occupy too much of your time. We will not use any of the results at Elbotola. Our aim is to have an
efficient and fair assessment: you spend time solving the challenge, and we spend time giving
you detailed feedback.

The assessment consists of two pages:

- Calendar Page to list the matches by date
- and Follow Page to list the followed teams

## Screenshots
![image info](./full_screenshot.png)


### Modules Required
- Preferably Kotlin (otherwise JAVA)
- No depenencies required, feel free to choose the stack you're confortable with

## Pages
### Calendar Page
- List Matches by date, grouped by competition and ordered by time,
- You render the match based on its status (match.match_details.match_status)
- Date Switcher
- Live Matches Toggle which shows currently playing matches if available, otherwise show an appropriate message
- You can follow a team when you click on the match model
- User uses Search Bar to filter competition names or team names


### Follow Page:
- List all Followed Teams
- You unfollow the team by toggling the star icon
- a search bar to filter the content

### API
https://run.mocky.io/v3/68d00bc4-7fa1-4ead-9167-093c56a772e3

This endpoint returns a list of random matches with to different dates

#### Schema:
```
match_id: string
time: string 
date: string 
competition: object
    id: string
    name: string
contestant: array
    id: string
    name: string
    position: string
match_details: object
    match_status: string
    winner: string
    scores: {
        ft: {
            home: number,
            away: number
        },
        total: {
            home: number,
            away: number
        }
    }
```

in order to display team logos you have to construct them using the following format:
`https://images.elbotola.com/stats/logos/{TEAM_ID}.png`
replace the **TEAM_ID** by the `contestant.id`

for competitions, use the following url
`https://images.elbotola.com/stats/competitions/{COMPETITON_ID}.png`
where **COMPETITON_ID** is `competition.id`

`match_details.match_status`: could have one of these possible values `Fixture`, `Playing`, `Played`, `Postponed`, `Cancelled`.
`match_details.scores.ft`: means final time score.
`match_details.scores.total`: mean the cumilative score for all match periods.


### Good luck :)
Once done, please commit your assement to a git repository and send us the link to android@elbotola.com with instructions on how to run the project
We'll review eventually your git workflow (commit, branches, pullrequests...).
